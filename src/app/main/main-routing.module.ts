import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ROUTING_NAMES } from '../constants/routing-names.constants';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MenuComponent } from './menu/menu.component';
import { SettingsComponent } from './settings/settings.component';

const routes: Routes = [
  {
    path: ROUTING_NAMES.MAIN_MODULE.DASHBOARD,
    component: DashboardComponent,
  },
  {
    path: ROUTING_NAMES.MAIN_MODULE.MENU,
    component: MenuComponent,
  },
  {
    path: ROUTING_NAMES.MAIN_MODULE.SETTINGS,
    component: SettingsComponent,
  },
];

export const listOfComponentsToDeclare = [DashboardComponent, MenuComponent, SettingsComponent];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MainPageRoutingModule {}
