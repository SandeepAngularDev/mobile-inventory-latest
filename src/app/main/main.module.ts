import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { listOfComponentsToDeclare, MainPageRoutingModule } from './main-routing.module';

@NgModule({
  declarations: [...listOfComponentsToDeclare],
  imports: [MainPageRoutingModule, SharedModule],
})
export class MainModule {}
