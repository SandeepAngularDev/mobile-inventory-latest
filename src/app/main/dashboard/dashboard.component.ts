import { Component, OnInit } from '@angular/core';
import { ModuleList } from 'src/app/interfaces/dashboard.interface';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  listOfModules: ModuleList[] = [
    {
      label: 'Put Away',
      icon: '',
      routingName: '/putAway',
      responsibilityName: '',
      status: {
        openCount: 0,
        completedCount: 0,
        progress: 0,
      },
      textToDisplayBesideStatus: {
        forOpen: `Open PO's`,
        forCompleted: `Completed Po's`,
      },
      queryToGetStatus: [
        {
          tableName: '',
          queryCondition: '',
          values: [],
        },
        {
          tableName: '',
          queryCondition: '',
          values: [],
          destinationTableName: '',
        },
      ],
    },
  ];

  constructor() {}

  ngOnInit() {}
}
