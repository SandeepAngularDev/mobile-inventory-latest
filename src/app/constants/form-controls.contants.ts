export const subGroupNames = {
  SUB_INV_LOC: 'subInvLoc',
  LOT_N_SERIAL: 'lotNSerial',
  DEST_SUB_INV_LOC: 'destSubInvLoc',
};

export const customFormControls = {
  Locator: subGroupNames.SUB_INV_LOC,
  SubInventoryCode: subGroupNames.SUB_INV_LOC,
  Lot: subGroupNames.LOT_N_SERIAL,
  Serial: subGroupNames.LOT_N_SERIAL,
  DestSubInventoryCode: subGroupNames.DEST_SUB_INV_LOC,
  DestLocator: subGroupNames.DEST_SUB_INV_LOC,
};
