const MAIN_MODULE_ROUTING_NAMES = {
  DASHBOARD: 'dashboard',
  MENU: 'menu',
  SETTINGS: 'settings',
};
export const ROUTING_NAMES = {
  MAIN_MODULE: MAIN_MODULE_ROUTING_NAMES,
};
