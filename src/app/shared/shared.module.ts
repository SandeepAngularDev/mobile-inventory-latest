import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { GlobalHeaderComponent } from './components/global-header/global-header.component';
import { IonicModule } from '@ionic/angular';
import { IonicStorageModule } from '@ionic/storage-angular';
import { AutoFocusPipe } from './pipes/auto-focus.pipe';
import { FormGroupDirective, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonSubInvLocSelectionComponent } from './components/common-sub-inv-loc-selection/common-sub-inv-loc-selection.component';
import { CommonLotSerialSelectionComponent } from './components/common-lot-serial-selection/common-lot-serial-selection.component';
import { CommonDestSubInvLocSelectionComponent } from './components/common-dest-sub-inv-loc-selection/common-dest-sub-inv-loc-selection.component';
import { GetTransactionStatusOfModulePipe } from './pipes/get-transaction-status-of-module.pipe';
import { GetTransactionBatchHistoryDetailsPipe } from './pipes/get-transaction-batch-history-details.pipe';
import { GetFormStatusPipe } from './pipes/get-form-status.pipe';
import { GetBooleanOnTruthyPipe } from './pipes/get-boolean-on-truthy.pipe';
import { CommonListSelectionPageComponent } from './components/common-list-selection-page/common-list-selection-page.component';
import { HideElementsPipe } from './pipes/hide-elements.pipe';

@NgModule({
  declarations: [
    GlobalHeaderComponent,
    AutoFocusPipe,
    CommonSubInvLocSelectionComponent,
    CommonLotSerialSelectionComponent,
    CommonDestSubInvLocSelectionComponent,
    GetTransactionStatusOfModulePipe,
    GetTransactionBatchHistoryDetailsPipe,
    GetFormStatusPipe,
    GetBooleanOnTruthyPipe,
    CommonListSelectionPageComponent,
    HideElementsPipe,
  ],
  imports: [CommonModule, IonicModule, TranslateModule, IonicStorageModule, FormsModule, ReactiveFormsModule],
  exports: [
    CommonModule,
    IonicStorageModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    TranslateModule,
    GlobalHeaderComponent,
    CommonSubInvLocSelectionComponent,
    CommonLotSerialSelectionComponent,
    CommonDestSubInvLocSelectionComponent,
    AutoFocusPipe,
    GetTransactionStatusOfModulePipe,
    GetTransactionBatchHistoryDetailsPipe,
    GetFormStatusPipe,
    GetBooleanOnTruthyPipe,
    HideElementsPipe,
    CommonListSelectionPageComponent,
  ],
})
export class SharedModule {}
