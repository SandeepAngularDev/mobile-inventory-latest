export type JsonMethodNames = 'stringify' | 'parse';
export type PromiseMethodNames = 'all' | 'race';
