import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { CacheService } from 'ionic-cache';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class StorageHandlerService {
  private ionicStorage: Storage | null = null;
  private isStorageExist = false;
  private storeKey = 'MSC_Store';

  constructor(private storage: Storage, private cache: CacheService) {
    console.log('StorageHandlerService', this.ionicStorage);
  }

  async init() {
    await this.doesStorageExist();
    if (this.isStorageExist) {
      const storage = await this.storage.create();
      await this.ionicStorage.set(this.storeKey, true);
      this.ionicStorage = storage;
    }
    return Promise.resolve(this.ionicStorage);
  }

  public set(key: string, value: any) {
    this.ionicStorage?.set(key, value);
  }

  async doesStorageExist() {
    try {
      const created = await this.ionicStorage.get(this.storeKey);
      if (created) {
        console.log(created, 'created');
        this.isStorageExist = true;
      }
    } catch (error) {
      console.log(error, 'error');
      this.isStorageExist = false;
    }
  }

  cacheData(key, obsr: Observable<any>) {
    this.cache.loadFromDelayedObservable(key, obsr);
  }
}
