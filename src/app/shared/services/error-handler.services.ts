import { Injectable, ErrorHandler } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ErrorhandlerService implements ErrorHandler {
  msg: string;
  arr = [];
  constructor() {}
  handleError(error: HttpErrorResponse) {
    console.log(error);
    return throwError(error);
  }
}
