import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { BehaviorSubject, ReplaySubject } from 'rxjs';
import { customFormControls, subGroupNames } from 'src/app/constants/form-controls.contants';
import { JsonMethodNames, PromiseMethodNames } from '../types/types';

@Injectable({
  providedIn: 'root',
})
export class UtilityService {
  isDestSubInventoryEnabled = new BehaviorSubject(false);
  isDestSubInventoryEnabled$ = this.isDestSubInventoryEnabled.asObservable();
  constructor(private fb: FormBuilder) {
    // this.promiseMethodUitility('all',[Promise.reject("errorgfcg")]);
    // this.jsonMethodUtility('stringify',{o:true})
  }

  async promiseMethodUitility(methodName: PromiseMethodNames, listOfPromisesToBeExecuted): Promise<Array<any>> {
    let errorDetail;
    const methodToCall: any = methodName;
    const listOfResolvedData = await Promise[`${methodToCall}`](listOfPromisesToBeExecuted).catch((err) => {
      errorDetail = err;
    });
    console.log(listOfResolvedData, errorDetail);
    return listOfResolvedData ? [listOfResolvedData, null] : [null, errorDetail];
  }

  jsonMethodUtility(methodName: JsonMethodNames, jsonDataToBeParsedOrStringified: string | Array<any> | object) {
    try {
      const jsonData = Object.is(methodName, 'parse')
        ? JSON.parse(`${jsonDataToBeParsedOrStringified}`)
        : JSON.stringify(jsonDataToBeParsedOrStringified);
      console.log(jsonData);
      return [jsonData, null];
    } catch (error) {
      console.log(error);

      return [null, error];
    }
  }

  createFormGroup(mainFormgroup: FormGroup) {
    const grpWiseFormControls = {};
    const subGroups = subGroupNames;
    if (subGroups) {
      for (const grp in subGroups) {
        if (subGroups.hasOwnProperty(grp)) {
          grpWiseFormControls[subGroups[grp]] = [];
          mainFormgroup.addControl(subGroups[grp], this.fb.group({}));
        }
      }
    }
    return {
      grpWiseFormControls,
    };
  }

  createFormControlsGroupWise(
    formFields: Array<any>,
    formControlDetails: object,
    mainFormgroup: FormGroup,
    grpWiseFormControlsDetails: object
  ) {
    const controls = formControlDetails;
    const grpWiseFormControls = grpWiseFormControlsDetails;
    for (const fieldInfo of formFields) {
      const subGrpName = controls[fieldInfo.DataSource];
      if (mainFormgroup.contains(subGrpName)) {
        grpWiseFormControls[subGrpName].push(fieldInfo);
        const subGrp = mainFormgroup.get(subGrpName) as FormGroup;
        console.log('subGrpName', subGrpName, fieldInfo.DataSource);
        subGrp.addControl(fieldInfo.DataSource, this.fb.control(fieldInfo.DefaultValue));
      }
    }
    return grpWiseFormControls;
  }

  async getSelectedInvOrgId() {
    return Promise.resolve('7964');
  }
}
