import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root',
})
export class LocalizationService {
  selected = '';
  LANG_KEY = 'SELECTED_LANGUAGE';
  constructor(private translate: TranslateService) {}

  async setInitialAppLanguage() {
    this.translate.setDefaultLang('de');
    const currentLang = 'de';
    //  await this.storage.get(this.LANG_KEY);
    const langKey = currentLang || 'de';
    return await this.setLanguage(langKey);
  }

  getLanguages() {
    return [
      { text: 'English', value: 'en', image: 'assets/img/en.png' },
      { text: 'German', value: 'de', image: 'assets/img/de.png' },
    ];
  }

  async setLanguage(lng) {
    console.log('executed');
    await this.translate.use(lng).toPromise();
    this.selected = lng;
    // this.storage.set(this.LANG_KEY, lng);
  }
}
