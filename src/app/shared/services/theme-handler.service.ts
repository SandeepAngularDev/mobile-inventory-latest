import { Injectable, Inject, Optional } from '@angular/core';
import * as ColorManipulator from 'color';
import { DOCUMENT } from '@angular/common';
import { from, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ThemeHandlerService {
  defaults = {
    primary: '#3880ff',
    secondary: '#0cd1e8',
    tertiary: '#7044ff',
    success: '#10dc60',
    warning: '#ffce00',
    danger: '#f04141',
    dark: '#222428',
    medium: '#989aa2',
    light: '#f4f5f8',
  };

  themesList = {
    autumn: {
      primary: '#F78154',
      secondary: '#4D9078',
      tertiary: '#B4436C',
      light: '#FDE8DF',
      medium: '#FCD0A2',
      dark: '#B89876',
    },
    night: {
      primary: '#8CBA80',
      secondary: '#FCFF6C',
      tertiary: '#FE5F55',
      medium: '#BCC2C7',
      dark: '#F7F7FF',
      light: '#495867',
    },
    neon: {
      primary: '#39BFBD',
      secondary: '#4CE0B3',
      tertiary: '#FF5E79',
      light: '#F4EDF2',
      medium: '#B682A5',
      dark: '#34162A',
    },
    darkmode: {
      primary: '#002F80',
      secondary: '#4CE0B3',
      tertiary: '#FF5E79',
      success: '#2dd36f',
      warning: '#FFC409',
      danger: '#F60424',
      dark: '#000000',
      medium: '#B682A5',
      light: '#FFFFFF',
    },
    crypton: {
      primary: '#0420F6',
      secondary: '#4CE0B3',
      tertiary: '#FF5E79',
      success: '#28BA62',
      warning: '#FFC409',
      danger: '#F60424',
      dark: '#222428',
      medium: '#808289',
      light: '#F4F5F8',
    },
    xeon: {
      primary: '#3968F3',
      secondary: '#4CE0B3',
      tertiary: '#FF5E79',
      success: '#01E009',
      warning: '#FFd500',
      danger: '#FA0000',
      dark: '#222428',
      medium: '#808289',
      light: '#F4F5F8',
    },
  };

  constructor(@Inject(DOCUMENT) private documentRef: Document) {
    //  localStorage.getItem('theme').then(cssText => {
    console.log(localStorage.getItem('theme'), 'get theme');
    of(localStorage.getItem('theme'))
      .toPromise()
      .then((cssText) => {
        // Storage.get('theme').then(cssText => {
        // <--- GET SAVED THEME
        console.log('before changes has occured');
        if (cssText) {
          console.log('cssText has changed');
          this.setGlobalCSS(cssText);
        }
      });
  }

  contrast(color, ratio = 0.8) {
    color = ColorManipulator(color);
    return color.isDark() ? color.lighten(ratio) : color.darken(ratio);
  }

  createNewCssStyles(colors) {
    colors = { ...this.defaults, ...colors };

    const { primary, secondary, tertiary, success, warning, danger, dark, medium, light } = colors;

    const shadeRatio = 0.1;
    const tintRatio = 0.1;

    return `
      --ion-color-base: ${light};
      --ion-color-contrast: ${dark};
      --ion-background-color:${light};
      --ion-text-color:${dark};
      --ion-toolbar-background-color:${this.contrast(light, 0.1)};
      --ion-toolbar-text-color:${this.contrast(dark, 0.1)};
      --ion-item-background-color:${this.contrast(light, 0.3)};
      --ion-item-text-color:${this.contrast(dark, 0.3)};

      --ion-color-primary: ${primary};
      --ion-color-primary-rgb: 56,128,255;
      --ion-color-primary-contrast: ${this.contrast(primary)};
      --ion-color-primary-contrast-rgb: 255,255,255;
      --ion-color-primary-shade:  ${ColorManipulator(primary).darken(shadeRatio)};

      --ion-color-secondary: ${secondary};
      --ion-color-secondary-rgb: 12,209,232;
      --ion-color-secondary-contrast: ${this.contrast(secondary)};
      --ion-color-secondary-contrast-rgb: 255,255,255;
      --ion-color-secondary-shade:  ${ColorManipulator(secondary).darken(shadeRatio)};

      --ion-color-tertiary: ${tertiary};
      --ion-color-tertiary-rgb: 112,68,255;
      --ion-color-tertiary-contrast: ${this.contrast(tertiary)};
      --ion-color-tertiary-contrast-rgb: 255,255,255;
      --ion-color-tertiary-shade:  ${ColorManipulator(tertiary).darken(shadeRatio)};

      --ion-color-success: ${success};
      --ion-color-success-rgb: 16,220,96;
      --ion-color-success-contrast: ${this.contrast(success)};
      --ion-color-success-contrast-rgb: 255,255,255;
      --ion-color-success-shade:  ${ColorManipulator(success).darken(shadeRatio)};

      --ion-color-warning: ${warning};
      --ion-color-warning-rgb: 255,213,52;
      --ion-color-warning-contrast: ${this.contrast(warning)};
      --ion-color-warning-contrast-rgb: 255,255,255;
      --ion-color-warning-shade:  ${ColorManipulator(warning).darken(shadeRatio)};

      --ion-color-danger: ${danger};
      --ion-color-danger-rgb: 255,73,97;
      --ion-color-danger-contrast: ${this.contrast(danger)};
      --ion-color-danger-contrast-rgb: 255,255,255;
      --ion-color-danger-shade:  ${ColorManipulator(danger).darken(shadeRatio)};


  `;
  }
  // Override all global variables with a new theme
  setTheme(theme) {
    console.log(`theme is ${theme}`);
    const cssText = this.createNewCssStyles(theme);
    this.setGlobalCSS(cssText);
  }

  // Define a single CSS variable
  setVariable(name, value) {
    this.documentRef.documentElement.style.setProperty(name, value);
  }

  private setGlobalCSS(css: string) {
    this.documentRef.documentElement.style.cssText = css;
  }
}
