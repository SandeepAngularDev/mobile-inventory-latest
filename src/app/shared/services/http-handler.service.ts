import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  HttpDeleteRequestI,
  HttpGetRequestI,
  HttpPostRequestI,
  HttpPutRequestI,
  HttpRequestInterface,
} from 'src/app/interfaces/http-request.interface';
import { HttpClient } from '@angular/common/http';
import { Observable, ReplaySubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class HttpHandlerService {
  url: string;
  headers?: HttpHeaders;
  responseDataType: any;
  requestQuery: string;
  mockData = new ReplaySubject(1);
  mockData$ = this.mockData.asObservable();

  constructor(private http: HttpClient) {}

  getData(httpConfig: HttpGetRequestI) {
    return this.http.get(httpConfig.url);
  }

  postData(httpConfig: HttpPostRequestI) {
    return this.http.post(httpConfig.url, httpConfig.body);
  }

  putData(httpConfig: HttpPutRequestI) {
    return this.http.put(httpConfig.url, httpConfig.body);
  }

  deleteData(httpConfig: HttpDeleteRequestI) {
    this.http.delete(httpConfig.url);
  }

  getHeaders() {
    const requestReaders = new HttpHeaders({
      'Content-Type': 'application/json',
      Accept: 'application/json',
      'Content-Language': 'en-US',
      Authorization: 'Basic c3lzYWRtaW46U3F1ZWV6ZUAzMjE=',
    });

    console.log(requestReaders);

    return requestReaders;
  }

  async getDocsForReceiving() {
    const data = await this.http
      .get(`/serverurl/EBS/20D/getDocumentsForReceiving/7964/''/'Y'`, {
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
          'Content-Language': 'en-US',
          Authorization: 'Basic c3lzYWRtaW46U3F1ZWV6ZUAzMjE=',
        },
      })
      .toPromise();
    console.log(data);
    return data;
  }

  async getSubInventories() {
    const data = await this.http
      .get(`/serverurl/EBS/20D/getSubinventories/7964/''/'Y'`, {
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
          'Content-Language': 'en-US',
          Authorization: 'Basic c3lzYWRtaW46U3F1ZWV6ZUAzMjE=',
        },
      })
      .toPromise();
    console.log(data);
    return data;
  }

  async getLocators() {
    const data = await this.http
      .get(`/serverurl/EBS/20D/getLocators/7964/''/'Y'`, {
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
          'Content-Language': 'en-US',
          Authorization: 'Basic c3lzYWRtaW46U3F1ZWV6ZUAzMjE=',
        },
      })
      .toPromise();
    console.log(data);
    return data;
  }

  async getRestrictedSubinventories() {
    const data = await this.http
      .get(`/serverurl/EBS/20D/getRestrictedSubinventories/7964/''/'Y'`, {
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
          'Content-Language': 'en-US',
          Authorization: 'Basic c3lzYWRtaW46U3F1ZWV6ZUAzMjE=',
        },
      })
      .toPromise();
    console.log(data);
    return data;
  }

  async getRestrictedLocators() {
    const data = await this.http
      .get(`/serverurl/EBS/20D/getRestrictedLocators/7964/''/'Y'`, {
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
          'Content-Language': 'en-US',
          Authorization: 'Basic c3lzYWRtaW46U3F1ZWV6ZUAzMjE=',
        },
      })
      .toPromise();
    console.log(data);
    return data;
  }
}
