import { Injectable } from '@angular/core';
import { SQLiteObject, SQLite } from '@awesome-cordova-plugins/sqlite/ngx';

@Injectable({
  providedIn: 'root',
})
export class DbService {
  private db: SQLiteObject;
  constructor(private sqlite: SQLite) {
    console.log('DbService', this.db);
  }

  async openDB() {
    try {
      if (!this.db) this.db = await this.sqlite.create({ name: 'Msc.db', location: 'default' });
      console.log(this.db, 'executed the creation of db');
    } catch (error) {
      console.error(error.message);
    }
    return Promise.resolve(this.db);
  }
}
