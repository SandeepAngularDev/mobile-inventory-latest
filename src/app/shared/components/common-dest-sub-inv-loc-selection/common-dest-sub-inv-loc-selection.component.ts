import { Component, Input, OnInit } from '@angular/core';
import {
  AbstractControl,
  AsyncValidatorFn,
  FormControl,
  FormGroup,
  FormGroupDirective,
  ValidationErrors,
} from '@angular/forms';
import { Observable, of } from 'rxjs';
import { HttpHandlerService } from '../../services/http-handler.service';
import { UtilityService } from '../../services/utility.service';

@Component({
  selector: 'app-common-dest-sub-inv-loc-selection',
  templateUrl: './common-dest-sub-inv-loc-selection.component.html',
  styleUrls: ['./common-dest-sub-inv-loc-selection.component.scss'],
})
export class CommonDestSubInvLocSelectionComponent implements OnInit {
  completeFormValues: any;
  subFormValues: any;
  listOfFormControls: any;
  isSubinventoryRestricted = false;
  isLocatorRestricted = false;
  fGName: string;
  subForm: FormGroup;
  locatorList: Array<any> = [];
  subInventoryList: Array<any> = [];
  restrcitedLocatorList: Array<any> = [];
  restrcitedSubInventoryList: Array<any> = [];
  enableDestSubInventory$: Observable<boolean> = of(false);
  sourceSubInvNLocatorsValues = {
    SubInventoryCode: '',
    Locator: '',
  };

  @Input('formGroupName') set formGroupName(value: string) {
    this.fGName = value;
  }
  @Input('formValues') set updateFormvalues(value: any) {
    this.subFormValues = value;
    // console.log(this.subFormValues, 'subFormValues');
  }
  @Input('mainFormValues') set updateMainFormvalues(value: any) {
    this.completeFormValues = value;
    this.sourceSubInvNLocatorsValues = this.completeFormValues.subInvLoc;
    // console.log(this.completeFormValues, 'completeFormValues');
  }
  @Input('formControlList') set getTheListOfFormControls(value: any) {
    this.listOfFormControls = value;
    // console.log(this.listOfFormControls, 'listOfFormControls');
  }
  @Input('isSubinventoryRestricted') set checkWhetherSubinventoryRestricted(value: boolean) {
    this.isSubinventoryRestricted = value;
    // console.log(this.isSubinventoryRestricted, 'isSubinventoryRestricted');
  }
  @Input('isLocatorRestricted') set checkWhetherLocatorRestricted(value: boolean) {
    this.isLocatorRestricted = value;
    // console.log(this.isLocatorRestricted, 'isLocatorRestricted');
  }

  constructor(
    private mainFG: FormGroupDirective,
    private utilityService: UtilityService,
    private httpHelperService: HttpHandlerService
  ) {
    this.enableDestSubInventory$ = this.utilityService.isDestSubInventoryEnabled$;
    this.httpHelperService.mockData$.subscribe((d) => {
      if (d) {
        this.locatorList = d[1].ActiveLocators;
        this.restrcitedLocatorList = d[2].RestrictedLocators;
        this.subInventoryList = d[3].ActiveSubInventories;
        this.restrcitedSubInventoryList = d[4].RestrictedSubInventories;
      }
    });
  }

  ngOnInit() {
    this.subForm = this.mainFG.control;
    // console.log(this.subForm, 'ss');
    this.addAsyncValidatorsToDestSubInvLocControls();
  }

  check() {
    console.log('working');
  }

  addAsyncValidatorsToDestSubInvLocControls() {
    if (this.subForm && this.fGName) {
      const destSubInvControl = this.getDestSubInvControl;
      const destLocControl = this.getDestLocatorControl;
      const validateDestSubInvCode = (): AsyncValidatorFn => {
        return (control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
          return this.validateDestSubIventoryCode();
        };
      };
      const validateDestLocatorCode = (): AsyncValidatorFn => {
        return (control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
          return this.validateDestLocator();
        };
      };

      if (!destSubInvControl.hasAsyncValidator(validateDestSubInvCode())) {
        destSubInvControl.setAsyncValidators(validateDestSubInvCode());
        destSubInvControl.updateValueAndValidity();
      }
      if (!destLocControl.hasAsyncValidator(validateDestLocatorCode())) {
        destLocControl.setAsyncValidators(validateDestLocatorCode());
        destLocControl.updateValueAndValidity();
      }
    }
  }

  get getDestSubInvControl() {
    const subInvLocForm = this.subForm.get(this.fGName) as FormGroup;
    const subInvControl = subInvLocForm.get('DestSubInventoryCode') as FormControl;
    return subInvControl;
  }
  get getDestLocatorControl() {
    const subInvLocForm = this.subForm.get(this.fGName) as FormGroup;
    const locControl = subInvLocForm.get('DestLocator') as FormControl;

    return locControl;
  }

  async validateDestSubIventoryCode() {
    const subInvCode = this.getDestSubInvControl?.value || '';
    const selectedInvOrgId = await this.utilityService.getSelectedInvOrgId();
    let listOfSubInvSelected = [];
    let subInvSelected = [];
    const onValidationError = { subInvIsInvalid: true };
    console.log(subInvCode, selectedInvOrgId, 'k', this.isSubinventoryRestricted);
    if (subInvCode && selectedInvOrgId) {
      if (this.isSubinventoryRestricted) {
        listOfSubInvSelected = this.restrcitedLocatorList.filter(
          (subInvDetails) =>
            Object.is(subInvDetails.SubInventoryCode, `${subInvCode}`) &&
            Object.is(subInvDetails.InventoryOrgId, `${selectedInvOrgId}`)
        );
        subInvSelected = await Promise.resolve(listOfSubInvSelected);
        return subInvSelected.length ? null : onValidationError;
      }
      console.log(subInvCode, selectedInvOrgId, 'came');
      listOfSubInvSelected = this.subInventoryList.filter(
        (subInvDetails) =>
          Object.is(subInvDetails.SubInventoryCode, `${subInvCode}`) &&
          Object.is(subInvDetails.InventoryOrgId, `${selectedInvOrgId}`)
      );
      subInvSelected = await Promise.resolve(listOfSubInvSelected);
      return subInvSelected.length ? null : onValidationError;
    }

    return onValidationError;
  }
  async validateDestLocator() {
    const destSubInvCode = this.getDestSubInvControl?.value || '';
    const destLocator = this.getDestLocatorControl?.value || '';
    const selectedInvOrgId = await this.utilityService.getSelectedInvOrgId();
    let listOfLocatorSelected = [];
    let locatorSelected = [];
    const onValidationError = { locatorIsInvalid: true };
    console.log(destSubInvCode, selectedInvOrgId, destLocator, 'locatorValidator');

    if (destSubInvCode && selectedInvOrgId && destLocator) {
      if (this.isLocatorRestricted) {
        listOfLocatorSelected = this.restrcitedLocatorList.filter(
          (locDetails) =>
            Object.is(locDetails.Locator, `${destLocator}`) &&
            Object.is(
              locDetails.InventoryOrgId,
              `${selectedInvOrgId}` && Object.is(locDetails.SubInventoryCode, `${destSubInvCode}`)
            )
        );
        locatorSelected = await Promise.resolve(listOfLocatorSelected);
        if (locatorSelected.length) {
          if (this.isItemMovedToSameSubInvLocator(destSubInvCode, destLocator)) {
            return onValidationError;
          }
          return null;
        }
        return onValidationError;
      }
      listOfLocatorSelected = this.locatorList.filter(
        (locDetails) =>
          Object.is(locDetails.Locator, `${destLocator}`) &&
          Object.is(locDetails.InventoryOrgId, `${selectedInvOrgId}`) &&
          Object.is(locDetails.SubInventoryCode, `${destSubInvCode}`)
      );
      locatorSelected = await Promise.resolve(listOfLocatorSelected);
      if (locatorSelected.length) {
        if (this.isItemMovedToSameSubInvLocator(destSubInvCode, destLocator)) {
          return onValidationError;
        }
        return null;
      }
      return onValidationError;
    }

    return onValidationError;
  }

  isItemMovedToSameSubInvLocator(destSubInv: string, destLocator: string): boolean {
    if (this.sourceSubInvNLocatorsValues) {
      if (Object.is(this.sourceSubInvNLocatorsValues.SubInventoryCode, destSubInv)) {
        if (Object.is(this.sourceSubInvNLocatorsValues.Locator, destLocator)) {
          return true;
        }
        return false;
      }
      return false;
    }
    return false;
  }
}
