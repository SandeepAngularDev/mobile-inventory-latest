import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { CommonListSelectionConfigI } from 'src/app/interfaces/common-component.interface';

@Component({
  selector: 'app-common-list-selection-page',
  templateUrl: './common-list-selection-page.component.html',
  styleUrls: ['./common-list-selection-page.component.scss'],
})
export class CommonListSelectionPageComponent implements OnInit {
  listOfFormControls: any;
  mocakJsonData = [
    { id: 1, add: 'sds', detail: 'oops' },
    { id: 2, add: 'kda', detail: 'noo!' },
  ];
  selctedDetails = {};

  @Input() detailsToDisplay: CommonListSelectionConfigI;
  @Input() listOfItems = [];

  // detailsToDisplay = {
  //   showSequence:true,
  //   startText: '',
  //   endText: 'End Text',
  //   listOfFieldsNamesToDisplay: ['add'],
  //   seperator: '',
  //   headerText:'Select',
  //   listOfRecordToHide : [],
  //   fieldNameToValidate: 'detail',
  //   multipleSelection : true
  // };

  constructor(public modalController: ModalController) {}

  ngOnInit() {
    // this.listOfItems = this.detailsToDisplay['listOfRecordsToDisplay'];
  }

  getCheckedItems(isChecked, index, value) {
    console.log(isChecked, 'isChecked');
    if (this.selctedDetails[index]) {
      delete this.selctedDetails[index];
    } else {
      if (Object.is(isChecked, true)) {
        this.selctedDetails[index] = value;
      }
    }
    console.log(value, index, this.selctedDetails);
  }

  getListOfSelectedItems() {
    console.log(Object.values(this.selctedDetails), 'whoooo!');
    this.dismiss(Object.values(this.selctedDetails));
  }
  getSelectedItem(selectedValue) {
    console.log(selectedValue);
    this.dismiss(selectedValue);
  }

  dismiss(dataToSendOnModalDismiss: any) {
    this.modalController.dismiss(dataToSendOnModalDismiss);
  }
}
