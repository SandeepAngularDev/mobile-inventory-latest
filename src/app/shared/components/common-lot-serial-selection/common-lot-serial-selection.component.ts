import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormGroupDirective } from '@angular/forms';

@Component({
  selector: 'app-common-lot-serial-selection',
  templateUrl: './common-lot-serial-selection.component.html',
  styleUrls: ['./common-lot-serial-selection.component.scss'],
})
export class CommonLotSerialSelectionComponent implements OnInit {
  completeFormValues: any;
  subFormValues: any;
  listOfFormControls: any;
  fGName: string;
  subForm: FormGroup;

  @Input('formGroupName') set formGroupName(value: string) {
    this.fGName = value;
  }
  @Input('formValues') set updateFormvalues(value: any) {
    this.subFormValues = value;
    // console.log(this.subFormValues, 'subFormValues');
  }
  @Input('mainFormValues') set updateMainFormvalues(value: any) {
    this.completeFormValues = value;
    // console.log(this.completeFormValues, 'completeFormValues');
  }
  @Input('formControlList') set getTheListOfFormControls(value: any) {
    this.listOfFormControls = value;
    // console.log(this.listOfFormControls, 'listOfFormControls');
  }

  constructor(private mainFG: FormGroupDirective) {}

  ngOnInit() {
    this.subForm = this.mainFG.control;
    // console.log(this.subForm, 'ss');
  }
}
