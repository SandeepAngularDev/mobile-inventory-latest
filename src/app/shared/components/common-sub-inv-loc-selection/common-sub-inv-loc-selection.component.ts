import { Component, Input, OnInit } from '@angular/core';
import {
  AbstractControl,
  AsyncValidatorFn,
  FormControl,
  FormGroup,
  FormGroupDirective,
  ValidationErrors,
} from '@angular/forms';
import { Observable, of } from 'rxjs';
import { HttpHandlerService } from '../../services/http-handler.service';
import { UtilityService } from '../../services/utility.service';
import { map } from 'rxjs/operators';
import { CommonListSelectionPageComponent } from '../common-list-selection-page/common-list-selection-page.component';
import { ModalController } from '@ionic/angular';
import { CommonListSelectionConfigI } from 'src/app/interfaces/common-component.interface';

@Component({
  selector: 'app-common-sub-inv-loc-selection',
  templateUrl: './common-sub-inv-loc-selection.component.html',
  styleUrls: ['./common-sub-inv-loc-selection.component.scss'],
})
export class CommonSubInvLocSelectionComponent implements OnInit {
  completeFormValues: any;
  subFormValues: any;
  listOfFormControls: Observable<any>;
  isSubinventoryRestricted = false;
  isLocatorRestricted = false;
  subForm: FormGroup;
  fGName: string;
  locatorList: Array<any> = [];
  subInventoryList: Array<any> = [];
  restrcitedLocatorList: Array<any> = [];
  restrcitedSubInventoryList: Array<any> = [];

  @Input('formGroupName') set formGroupName(value: string) {
    this.fGName = value;
    console.log('1st');
  }
  @Input('formValues') set updateFormvalues(value: any) {
    this.subFormValues = value;
    console.log(this.subFormValues, 'subFormValues');
  }
  @Input('mainFormValues') set updateMainFormvalues(value: any) {
    this.completeFormValues = value;
    console.log(this.completeFormValues, 'completeFormValues');
  }
  @Input('formControlList') set getTheListOfFormControls(value: any) {
    this.listOfFormControls = of(value);
    if (this.subFormValues.SubInventoryCode) {
      this.utilityService.isDestSubInventoryEnabled.next(true);
      this.listOfFormControls.pipe(
        map((controls) => {
          console.log(controls, 'controls');
          controls[this.fGName].isVisibile = true;
          return controls;
        })
      );
    }
    console.log(this.listOfFormControls, 'listOfFormControls');
  }
  @Input('isSubinventoryRestricted') set checkWhetherSubinventoryRestricted(value: boolean) {
    this.isSubinventoryRestricted = value;
    console.log(this.isSubinventoryRestricted, 'isSubinventoryRestricted');
  }
  @Input('isLocatorRestricted') set checkWhetherLocatorRestricted(value: boolean) {
    this.isLocatorRestricted = value;
    console.log(this.isLocatorRestricted, 'isLocatorRestricted');
  }
  constructor(
    private mainFG: FormGroupDirective,
    private utilityService: UtilityService,
    private httpHelperService: HttpHandlerService,
    public modalController: ModalController
  ) {
    this.httpHelperService.mockData$.subscribe((d) => {
      if (d) {
        this.locatorList = d[1].ActiveLocators;
        this.restrcitedLocatorList = d[2].RestrictedLocators;
        this.subInventoryList = d[3].ActiveSubInventories;
        this.restrcitedSubInventoryList = d[4].RestrictedSubInventories;
      }
    });
  }

  ngOnInit() {
    this.subForm = this.mainFG.control as FormGroup;

    const subInvLocForm = this.subForm.get(this.fGName) as FormGroup;
    subInvLocForm.statusChanges.subscribe((d) => {
      console.log(d, 'status');
    });
    subInvLocForm.valueChanges.subscribe((d) => {
      console.log(d, 'values');
      console.log(subInvLocForm, 'check');
    });

    this.addAsyncValidatorsToSubInvLocControls();
  }

  check() {
    console.log('working');
  }
  async validateSubIventoryCode() {
    const subInvCode = this.getSubInvControl?.value || '';
    const selectedInvOrgId = await this.utilityService.getSelectedInvOrgId();
    let listOfSubInvSelected = [];
    let subInvSelected = [];
    const onValidationError = { subInvIsInvalid: true };
    console.log(subInvCode, selectedInvOrgId, 'k', this.isSubinventoryRestricted);
    if (subInvCode && selectedInvOrgId) {
      if (this.isSubinventoryRestricted) {
        listOfSubInvSelected = this.restrcitedLocatorList.filter(
          (subInvDetails) =>
            Object.is(subInvDetails.SubInventoryCode, `${subInvCode}`) &&
            Object.is(subInvDetails.InventoryOrgId, `${selectedInvOrgId}`)
        );
        subInvSelected = await Promise.resolve(listOfSubInvSelected);
        return subInvSelected.length ? null : onValidationError;
      }
      console.log(subInvCode, selectedInvOrgId, 'came');
      listOfSubInvSelected = this.subInventoryList.filter(
        (subInvDetails) =>
          Object.is(subInvDetails.SubInventoryCode, `${subInvCode}`) &&
          Object.is(subInvDetails.InventoryOrgId, `${selectedInvOrgId}`)
      );
      subInvSelected = await Promise.resolve(listOfSubInvSelected);
      return subInvSelected.length ? null : onValidationError;
    }

    return onValidationError;
  }
  async validateLocator() {
    const subInvCode = this.getSubInvControl?.value || '';
    const locator = this.getLocatorControl?.value || '';
    const selectedInvOrgId = await this.utilityService.getSelectedInvOrgId();
    let listOfLocatorSelected = [];
    let locatorSelected = [];
    const onValidationError = { locatorIsInvalid: true };
    console.log(subInvCode, selectedInvOrgId, locator, 'locatorValidator');

    if (subInvCode && selectedInvOrgId && locator) {
      if (this.isLocatorRestricted) {
        listOfLocatorSelected = this.restrcitedLocatorList.filter(
          (locDetails) =>
            Object.is(locDetails.Locator, `${locator}`) &&
            Object.is(
              locDetails.InventoryOrgId,
              `${selectedInvOrgId}` && Object.is(locDetails.SubInventoryCode, `${subInvCode}`)
            )
        );
        locatorSelected = await Promise.resolve(listOfLocatorSelected);
        return locatorSelected.length ? null : onValidationError;
      }
      listOfLocatorSelected = this.locatorList.filter(
        (locDetails) =>
          Object.is(locDetails.Locator, `${locator}`) &&
          Object.is(locDetails.InventoryOrgId, `${selectedInvOrgId}`) &&
          Object.is(locDetails.SubInventoryCode, `${subInvCode}`)
      );
      locatorSelected = await Promise.resolve(listOfLocatorSelected);
      return locatorSelected.length ? null : onValidationError;
    }

    return onValidationError;
  }

  async test() {
    const res = await this.validateSubIventoryCode();
    console.log(res, 'whoo!');
  }

  addAsyncValidatorsToSubInvLocControls() {
    if (this.subForm && this.fGName) {
      const subInvLocForm = this.subForm.get(this.fGName) as FormGroup;
      const subInvControl = subInvLocForm.get('SubInventoryCode') as FormControl;
      const locControl = subInvLocForm.get('Locator') as FormControl;
      const validateSubInvCode = (): AsyncValidatorFn => {
        return (control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
          return this.validateSubIventoryCode();
        };
      };
      const validateLocatorCode = (): AsyncValidatorFn => {
        return (control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
          return this.validateLocator();
        };
      };

      if (!subInvControl.hasAsyncValidator(validateSubInvCode())) {
        subInvControl.setAsyncValidators(validateSubInvCode());
        subInvControl.updateValueAndValidity();
      }
      if (!locControl.hasAsyncValidator(validateLocatorCode())) {
        locControl.setAsyncValidators(validateLocatorCode());
        locControl.updateValueAndValidity();
      }
    }
  }

  get getSubInvControl() {
    const subInvLocForm = this.subForm.get(this.fGName) as FormGroup;
    const subInvControl = subInvLocForm.get('SubInventoryCode') as FormControl;
    return subInvControl;
  }
  get getLocatorControl() {
    const subInvLocForm = this.subForm.get(this.fGName) as FormGroup;
    const locControl = subInvLocForm.get('Locator') as FormControl;

    return locControl;
  }

  async presentModal(listPageConfig) {
    const modal = await this.modalController.create({
      component: CommonListSelectionPageComponent,
      componentProps: {
        detailsToDisplay: listPageConfig,
      },
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    console.log(data);
    return data;
  }

  async openModalForLocator() {
    const subInvCode = this.getSubInvControl?.value || '';
    const selectedInvOrgId = await this.utilityService.getSelectedInvOrgId();
    const listPageConfig: CommonListSelectionConfigI = {
      showSequence: true,
      startText: '',
      endText: '',
      listOfFieldsNamesToDisplay: ['Locator', 'LocatorId'],
      seperator: '-',
      headerText: 'Select Locator',
      listOfRecordToHide: [],
      fieldNameToValidate: 'Locator',
      multipleSelection: false,
      listOfRecordsToDisplay: this.locatorList.filter(
        (locDetails) =>
          Object.is(locDetails.InventoryOrgId, `${selectedInvOrgId}`) &&
          Object.is(locDetails.SubInventoryCode, `${subInvCode}`)
      ),
    };
    const selectedRecord = await this.presentModal(listPageConfig);
    console.log(selectedRecord, 'selectedRecord');
    if (selectedRecord) {
      this.getLocatorControl.patchValue(selectedRecord.Locator);
    }
  }

  async openModalForSubInv() {
    const selectedInvOrgId = await this.utilityService.getSelectedInvOrgId();
    const listPageConfig: CommonListSelectionConfigI = {
      showSequence: true,
      startText: '',
      endText: '',
      listOfFieldsNamesToDisplay: ['SubInventoryCode'],
      seperator: '-',
      headerText: 'Select SubInventory',
      listOfRecordToHide: [],
      fieldNameToValidate: 'SubInventoryCode',
      multipleSelection: false,
      listOfRecordsToDisplay: this.subInventoryList.filter((locDetails) =>
        Object.is(locDetails.InventoryOrgId, `${selectedInvOrgId}`)
      ),
    };
    const selectedRecord = await this.presentModal(listPageConfig);
    console.log(selectedRecord, 'selectedRecord');
    if (selectedRecord) {
      this.getSubInvControl.patchValue(selectedRecord.SubInventoryCode);
    }
  }

  async openModalForSelection(formControlName: string) {
    if (Object.is(formControlName, 'Locator')) {
      await this.openModalForLocator();
    } else {
      await this.openModalForSubInv();
    }
  }
}
