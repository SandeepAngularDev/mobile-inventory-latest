import { Pipe, PipeTransform } from '@angular/core';
import { Observable, of } from 'rxjs';

@Pipe({
  name: 'getTransactionBatchHistoryDetails',
})
export class GetTransactionBatchHistoryDetailsPipe implements PipeTransform {
  transform(value: unknown, ...args: unknown[]): Observable<any> {
    return of();
  }
}
