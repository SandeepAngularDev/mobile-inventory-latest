import { Pipe, PipeTransform } from '@angular/core';
import { UtilityService } from '../services/utility.service';
import { IonInput } from '@ionic/angular';

@Pipe({
  name: 'autoFocus',
})
export class AutoFocusPipe implements PipeTransform {
  constructor() {}

  async transform(value: string, arg: HTMLIonInputElement, canFocus?: boolean) {
    console.log(value, 'value');
    console.log(arg, 'arg');
    const c = await arg.setFocus();

    return value;
  }
}
