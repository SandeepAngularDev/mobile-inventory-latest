import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'getBooleanOnTruthy',
})
export class GetBooleanOnTruthyPipe implements PipeTransform {
  transform(value: string | boolean | number): boolean {
    const isTruthy = [true, 'true', 'True', 'valid', 1, '1', 'VALID'].includes(value);
    console.log(value, 'coming status', isTruthy);
    return isTruthy ? true : false;
  }
}
