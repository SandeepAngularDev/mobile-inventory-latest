import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'hideElements',
})
export class HideElementsPipe implements PipeTransform {
  transform(fieldName: string, valueToCompare: string | number, listOfRecordToHide: any[]): unknown {
    let filteredRecordList = [];
    console.log(fieldName, valueToCompare, listOfRecordToHide);
    if (fieldName && valueToCompare && listOfRecordToHide.length) {
      filteredRecordList = listOfRecordToHide.filter((record) => {
        return Object.is(`${record[fieldName]}`, `${valueToCompare}`);
      });
      return filteredRecordList.length ? true : false;
    }
    return false;
  }
}
