import { Pipe, PipeTransform } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import {
  QueryToGetNoOfRecords,
  QueryToGetStatus,
  TransactionStatusOfModule,
} from 'src/app/interfaces/dashboard.interface';

@Pipe({
  name: 'getTransactionStatusOfModule',
})
export class GetTransactionStatusOfModulePipe implements PipeTransform {
  transform(
    initialStatusDetails: TransactionStatusOfModule,
    queryDetailToGetStatus: QueryToGetStatus[] | QueryToGetNoOfRecords[]
  ): Observable<TransactionStatusOfModule> {
    const noOfQueriesToExecute = queryDetailToGetStatus ? queryDetailToGetStatus.length : 0;
    if (noOfQueriesToExecute) {
      const queryToGetOpenCount = queryDetailToGetStatus[0];
      let queryToGetCompletedCount;
      let getStatusOfTheModule = of([]);
      if (Object.is(noOfQueriesToExecute, 1) && queryToGetOpenCount['isFullyReceivedCountExistInQuery']) {
        return this.getTransactionStatusOfModule(queryToGetOpenCount);
      }
      getStatusOfTheModule =
        queryToGetOpenCount && queryToGetOpenCount['query']
          ? this.getNoOfRecords(queryToGetOpenCount['query'])
          : this.selectLocalCount(queryToGetOpenCount);

      if (queryDetailToGetStatus[1]) {
        queryToGetCompletedCount = queryDetailToGetStatus[1];
      }

      const moduleStatus = getStatusOfTheModule.pipe(
        switchMap((openCountValue) => {
          const openCount = openCountValue && openCountValue.length ? openCountValue.length : 0;
          const getCount =
            queryToGetCompletedCount && queryToGetCompletedCount.destinationTableName
              ? this.selectCompletedCount(queryToGetCompletedCount)
              : this.selectLocalCount(queryToGetCompletedCount);
          return getCount.pipe(
            map((completedCountValue) => {
              const completedCount =
                completedCountValue && completedCountValue.length ? completedCountValue[0].count : 0;
              const progress = Math.round((completedCount * 100) / openCount) || 0;
              return {
                openCount,
                completedCount,
                progress,
              };
            })
          );
        })
      );

      return moduleStatus;
    }
    return of(initialStatusDetails);
  }

  getNoOfRecords(query: string) {
    return of([5]);
  }

  selectCompletedCount(queryDetail: QueryToGetStatus | QueryToGetNoOfRecords) {
    return of([{ count: 0 }]);
  }

  selectLocalCount(queryDetail: QueryToGetStatus | QueryToGetNoOfRecords) {
    return of([{ count: 0 }]);
  }
  getTransactionStatusOfModule(queryDetail: QueryToGetStatus | QueryToGetNoOfRecords) {
    let completed = 0;
    let total = 0;
    let percentage = 0;

    const statusOftheModule = of([]).pipe(
      switchMap((openItemsList) => {
        if (openItemsList) {
          let totalPosCount = 0;
          for (const po of openItemsList) {
            totalPosCount += +`${po.TotalCount}`;
            if (+po.FullyReceivedCount && Object.is(+po.FullyReceivedCount, +po.TotalCount)) {
              completed = completed + 1;
            }
          }
          total = totalPosCount - completed;
        } else {
          completed = 0;
          total = 0;
        }
        const progress: number = completed ? Math.round((completed * 100) / total) : 0;
        percentage = progress;

        return of({
          openCount: total,
          completedCount: completed,
          progress: percentage,
        });
      })
    );

    return statusOftheModule;
  }
}
