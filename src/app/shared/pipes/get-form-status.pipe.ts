import { Pipe, PipeTransform } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';

@Pipe({
  name: 'getFormStatus',
})
export class GetFormStatusPipe implements PipeTransform {
  transform(mainFormGrp: FormGroup, formGroupName: string): FormGroup {
    const subForm = mainFormGrp.get(formGroupName) as FormGroup;
    return subForm;
  }
}
