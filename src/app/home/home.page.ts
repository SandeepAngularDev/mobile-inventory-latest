import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { customFormControls, subGroupNames } from '../constants/form-controls.contants';
import { CommonListSelectionConfigI } from '../interfaces/common-component.interface';
import { CommonListSelectionPageComponent } from '../shared/components/common-list-selection-page/common-list-selection-page.component';
import { HttpHandlerService } from '../shared/services/http-handler.service';
import { UtilityService } from '../shared/services/utility.service';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  public envVariables = environment;
  pipeText = 'demo.title';
  checkDynamicTextTranslate = 'demo.text';
  listOfFormControlsOnSubgrp = {};
  docList: Array<any> = [];
  selectedItemOfPo: any;
  fields = [
    {
      Field: 'SUB_INV',
      DataSource: 'SubInventoryCode',
      Label: 'Select SubInventoryCode',
      Type: 'String',
      Length: '20',
      Editable: 'false',
      Position: '1',
      Scannable: true,
      DefaultValue: 'C001',
      Required: true,
      DisplayType: 'text',
      isVisibile: true,
    },
    {
      Field: 'LOCATOR',
      DataSource: 'Locator',
      Label: 'Select Locator',
      Type: 'String',
      Length: '20',
      Editable: true,
      Position: '2',
      Scannable: true,
      DefaultValue: '',
      Required: true,
      DisplayType: 'text',
      isVisibile: false,
    },
    {
      Field: 'LOT',
      DataSource: 'Lot',
      Label: 'Select Lot',
      Type: 'String',
      Length: '20',
      Editable: true,
      Position: '2',
      Scannable: true,
      DefaultValue: '',
      Required: true,
      DisplayType: 'text',
      isVisibile: true,
    },
    {
      Field: 'SERIAL',
      DataSource: 'Serial',
      Label: 'Select Serial',
      Type: 'String',
      Length: '20',
      Editable: 'false',
      Position: '1',
      Scannable: true,
      DefaultValue: 'SER123',
      Required: true,
      DisplayType: 'text',
      isVisibile: true,
    },

    {
      Field: 'DEST_SUB_INV',
      DataSource: 'DestSubInventoryCode',
      Label: 's Dest SubInventoryCode',
      Type: 'String',
      Length: '20',
      Editable: 'false',
      Position: '1',
      Scannable: true,
      DefaultValue: 'C001',
      Required: true,
      DisplayType: 'textdetail',
      isVisibile: true,
    },
    {
      Field: 'DEST_LOCATOR',
      DataSource: 'DestLocator',
      Label: 'Select Dest Locator',
      Type: 'String',
      Length: '20',
      Editable: true,
      Position: '2',
      Scannable: true,
      DefaultValue: '',
      Required: true,
      DisplayType: 'text',
      isVisibile: true,
    },
  ];
  mainFormgroup: FormGroup;
  subGroupNameList = Object.entries(subGroupNames);
  requiredData;
  listPageConfig: CommonListSelectionConfigI = {
    showSequence: true,
    startText: '',
    endText: 'End Text',
    listOfFieldsNamesToDisplay: ['add'],
    seperator: '',
    headerText: 'Select',
    listOfRecordToHide: [{ id: 1, add: 'sds', detail: 'oops' }],
    fieldNameToValidate: 'detail',
    multipleSelection: true,
    listOfRecordsToDisplay: [],
  };
  constructor(
    private fb: FormBuilder,
    private utilityService: UtilityService,
    private httpHelperService: HttpHandlerService,
    private router: Router,
    public modalController: ModalController
  ) {}

  ngOnInit() {
    this.httpHelperService.mockData$.subscribe((d) => {
      this.requiredData = d;
      console.log(this.requiredData, 'requiredData');
    });
    this.mainFormgroup = this.fb.group({});
    this.mainFormgroup.valueChanges.subscribe(console.log);
    this.createFormControls(this.fields);
    setTimeout(() => {
      // this.router.navigate(['/dashboard']);
    }, 30000);
    const test = of(5).pipe(
      switchMap((first) => {
        return of(2).pipe(
          map((second) => {
            return {
              finalRes: second * first,
            };
          })
        );
      })
    );
    test.subscribe((final) => {
      console.log(final, 'final');
    });
  }

  // createFormControls(formControlList:any[]) {
  //   const controls = customFormControls;
  //   const grpWiseFormControls = {};
  //   for(const grp in subGroupNames){
  //     grpWiseFormControls[subGroupNames[grp]] = [];
  //     this.mainFormgroup.addControl(subGroupNames[grp],this.fb.group({}));
  //   }
  //   for(const fieldInfo of this.fields){
  //        const subGrpName =controls[fieldInfo['DataSource']];
  //        if(this.mainFormgroup.contains(subGrpName)){
  //         grpWiseFormControls[subGrpName].push(fieldInfo);
  //         const subGrp =this.mainFormgroup.get(subGrpName) as FormGroup;
  //         console.log('subGrpName',subGrpName,fieldInfo['DataSource']);
  //         subGrp.addControl(fieldInfo['DataSource'],this.fb.control(fieldInfo['DefaultValue']));
  //      }
  //   }

  //   this.listOfFormControlsOnSubgrp = grpWiseFormControls;

  //  console.log(this.mainFormgroup,'check',grpWiseFormControls);
  // }

  createFormControls(formControlList: any[]) {
    const controls = customFormControls;
    const { grpWiseFormControls } = this.utilityService.createFormGroup(this.mainFormgroup);
    console.log(grpWiseFormControls, 'grpWiseFormControls');
    console.log(this.mainFormgroup, 'mainFormgroup');

    const grpWiseFormControlsDetails = this.utilityService.createFormControlsGroupWise(
      this.fields,
      controls,
      this.mainFormgroup,
      grpWiseFormControls
    );

    this.listOfFormControlsOnSubgrp = grpWiseFormControlsDetails;

    console.log(this.mainFormgroup, 'check', this.listOfFormControlsOnSubgrp);
  }

  getDocList(ele) {
    console.log(ele, 'ele', ele?.value);
    const docs = this.requiredData[0].Docs4Receiving as Array<any>;
    this.docList = docs.filter((e) => Object.is(`${e.PoNumber}`, `${ele.value}`));
    console.log(this.docList, 'docsList');
  }
  getDoc(ele) {
    console.log(ele, 'ele', ele?.value);
    this.selectedItemOfPo = ele?.value;
  }

  async presentModal() {
    console.log(typeof this.listPageConfig, 'type');
    const modal = await this.modalController.create({
      component: CommonListSelectionPageComponent,
      componentProps: {
        detailsToDisplay: this.listPageConfig,
      },
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    console.log(data);
  }
}
