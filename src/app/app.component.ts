import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { LocalizationService } from './shared/services/localization.service';
import { UtilityService } from './shared/services/utility.service';
import { Storage } from '@ionic/storage';
import { StorageHandlerService } from './shared/services/storage-handler.service';
import { ThemeHandlerService } from './shared/services/theme-handler.service';
import { HttpHandlerService } from './shared/services/http-handler.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  data;
  constructor(
    private http: HttpClient,
    private localizationService: LocalizationService,
    private platform: Platform,
    private storage: Storage,
    private store: StorageHandlerService,
    private themeServ: ThemeHandlerService,
    private httpHelperService: HttpHandlerService
  ) {}

  ngOnInit() {
    setTimeout(async () => {
      this.data = await Promise.all([
        this.httpHelperService.getDocsForReceiving(),
        this.httpHelperService.getLocators(),
        this.httpHelperService.getRestrictedLocators(),
        this.httpHelperService.getSubInventories(),
        this.httpHelperService.getRestrictedSubinventories(),
      ]);

      console.log(this.data);
      this.httpHelperService.mockData.next(this.data);
    }, 25000);
    //     this.platform.ready().then(async d=>{
    // await this.store.init();
    //     })
    const themeList = Object.keys(this.themeServ.themesList);
    setInterval(() => {
      const themeIndex = Math.floor(Math.random() * themeList.length);
      this.themeServ.setTheme(this.themeServ.themesList[`${themeList[themeIndex]}`]);
    }, 25000);
  }
}
