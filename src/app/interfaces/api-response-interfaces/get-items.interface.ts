export interface GetItemResponseI {
  items: ItemDetailI[];
  success: boolean;
}

export interface ItemDetailI {
  CategoryId: number;
  CategorySetDesc: string;
  CategorySetId: number;
  CategorySetName: string;
  CurrencyCode: string;
  Flag: string;
  InventoryOrgId: number;
  IsLocatorRestricted: boolean;
  IsLotControlled: boolean;
  IsSerialControlled: boolean;
  IsSubinventoryRestricted: boolean;
  ItemCategory: string;
  ItemCategoryDesc: string;
  ItemDesc: string;
  ItemId: number;
  ItemNumber: string;
  ItemPrice: number;
  LastUpdateDate: string;
  ManufacturerName: string;
  ManufacturerPartNum: number;
  MaxOrderQty: number;
  MinMaxMaxQty: number;
  MinMaxMinQty: number;
  MinOrderQty: number;
  PlanningMethod: string;
  PrimaryUom: string;
  SecondaryUom: string;
  SerialTypeCode: number;
  SerialTypeDesc: string;
}
