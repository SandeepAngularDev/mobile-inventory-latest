import { HttpHeaders } from '@angular/common/http';

export type HttpGetRequestI = HttpRequestInterface;
export interface HttpPostRequestI extends HttpRequestInterface {
  body: any;
}
export interface HttpPutRequestI extends HttpRequestInterface {
  body: any;
}
export type HttpDeleteRequestI = HttpRequestInterface;

export interface HttpRequestInterface {
  url: string;
  headers?: HttpHeaders;
  responseDataType: any;
}
