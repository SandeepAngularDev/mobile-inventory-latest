export interface CommonListSelectionConfigI {
  showSequence: boolean;
  startText: string;
  endText: string;
  listOfFieldsNamesToDisplay: string[];
  seperator: string;
  headerText: string;
  listOfRecordToHide: any[];
  fieldNameToValidate: string;
  multipleSelection: boolean;
  listOfRecordsToDisplay: any[];
}
