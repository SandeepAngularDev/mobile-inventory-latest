export interface TransactionStatusOfModule {
  openCount: number;
  completedCount: number;
  progress: number;
}

export interface TextToDisplayBesideStatus {
  forOpen: string;
  forCompleted: string;
}

export interface QueryToGetStatus {
  tableName: string;
  queryCondition: string;
  values: any[];
  destinationTableName?: string;
}

export interface QueryToGetNoOfRecords {
  query: string;
  isFullyReceivedCountExistInQuery?: boolean;
}

export interface ModuleList {
  label: string;
  icon: string;
  routingName: string;
  responsibilityName: string;
  status: TransactionStatusOfModule;
  textToDisplayBesideStatus: TextToDisplayBesideStatus;
  queryToGetStatus: QueryToGetStatus[] | QueryToGetNoOfRecords[];
}
