import { APP_INITIALIZER, ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SharedModule } from './shared/shared.module';
import { ErrorhandlerService } from './shared/services/error-handler.services';
import { InterceptorService } from './shared/services/interceptor.service';
import { IonicStorageModule } from '@ionic/storage-angular';
import { LocalizationService } from './shared/services/localization.service';
import { DbService } from './shared/services/db.service';
import { StorageHandlerService } from './shared/services/storage-handler.service';
import { SQLite } from '@awesome-cordova-plugins/sqlite/ngx';
import { Drivers } from '@ionic/storage';
import { CacheModule } from 'ionic-cache';

export const createTranslateLoader = (http: HttpClient) => {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
};

export const appInitializationLoader = (localizationService: LocalizationService) => {
  return () => {
    console.log('Loads on app initialization');
    return localizationService.setInitialAppLanguage();
  };
};

export const appDbInitializationLoader = (dbSer: DbService) => {
  return () => {
    console.log('Loads on app db initialization');
    return dbSer.openDB();
  };
};
export const appStorageInitializationLoader = (storageService: StorageHandlerService) => {
  return () => {
    console.log('Loads on app storage initialization');
    return storageService.init();
  };
};

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
    }),
    SharedModule,
    IonicStorageModule.forRoot({
      name: 'MSC_DB',
      driverOrder: [Drivers.IndexedDB, Drivers.LocalStorage],
    }),
    CacheModule.forRoot(),
  ],
  providers: [
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: ErrorHandler, useClass: ErrorhandlerService },
    { provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true },
    { provide: APP_INITIALIZER, useFactory: appInitializationLoader, deps: [LocalizationService], multi: true },
    // { provide: APP_INITIALIZER, useFactory: appDbInitializationLoader, deps: [DbService], multi: true },
    // { provide: APP_INITIALIZER, useFactory: appStorageInitializationLoader, deps: [StorageHandlerService], multi: true },
    SQLite,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
