import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'Mobile Inventory Latest',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
